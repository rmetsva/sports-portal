const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
    event_name: {
        type: String,
        required: true
    },
    participants: {
        type: [{
            type: String
        }]
    },
    results: [{
        athletes_place: {
            type: Number,
            required: true
        },
        athletes_name: {
            type: String,
            required: true
        }
    }]
});

const Event = module.exports = mongoose.model('Event', EventSchema);

module.exports.getEventByEvent_name = (event_name, callback) => {
    const query = { event_name: event_name }
    Event.findOne(query, callback);
}

module.exports.addEvent = (newEvent, callback) => {
    newEvent.save(callback);
}

module.exports.addResults = (event_name, results, callback) => {
    Event.getEventByEvent_name(event_name, (err, event) => {
        if (err) throw err;
        event.results = results;
        event.save(callback);
    });
}
