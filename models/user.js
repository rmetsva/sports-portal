const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const mongooseObserver = require('mongoose-observer');

const Event = require('../models/event');

const UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    predictions: [{
        predicted_event: {
            type: String,
            required: true
        },
        predicted_winner: {
            type: String,
            required: true
        },
        points_for_prediction: {
            type: Number,
            default: 0
        }
    }],
    points: {
        points_total: {
            type: Number,
            default: 0
        },
        points_per_places: {
            points_per_first: {
                type: Number,
                default: 0
            },
            points_per_second: {
                type: Number,
                default: 0
            },
            points_per_third: {
                type: Number,
                default: 0
            },
            points_per_fourth: {
                type: Number,
                default: 0
            },
            points_per_fifth: {
                type: Number,
                default: 0
            },
            points_per_sixth: {
                type: Number,
                default: 0
            },
            points_per_seventh: {
                type: Number,
                default: 0
            },
            points_per_eighth: {
                type: Number,
                default: 0
            }
        }
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = (id, callback) => {
    User.findById(id, callback);
}

module.exports.getUserByUsername = (username, callback) => {
    const query = { username: username }
    User.findOne(query, callback);
}

module.exports.addUser = (newUser, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.comparePassword = (candidate, hash, callback) => {
    bcrypt.compare(candidate, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
}

module.exports.addPredictions = (user_id, predictions, callback) => {
    User.getUserById(user_id, (err, user) => {
        if (err) throw err;
        user.predictions = predictions;
        user.save(callback);
    });
}

module.exports.changePredictions = (user_id, new_predictions, callback) => {
    User.getUserById(user_id, (err, user) => {
        if (err) throw err;
        new_predictions.forEach(new_prediction => {
            for (let i = 0; i < user.predictions.length; i++) {
                if (new_prediction.predicted_event === user.predictions[i].predicted_event) {
                    user.predictions[i].predicted_winner = new_prediction.predicted_winner;
                    break;
                }
            }
        });
        user.save(callback);
    });
}

module.exports.calculatePoints = (event_name) => {
    User.find().exec((err, docs) => {
        if (err) throw err;
        docs.forEach(doc => {
            doc.predictions.forEach(prediction => {
                if (prediction.predicted_event === event_name) {
                    let initial_points = prediction.points_for_prediction;
                    prediction.points_for_prediction = 0;
                    doc.points.points_total -= initial_points;
                    if (initial_points === 10) {
                        doc.points.points_per_places.points_per_first -= initial_points;
                    } else if (initial_points === 8) {
                        doc.points.points_per_places.points_per_second -= initial_points;
                    } else if (initial_points === 6) {
                        doc.points.points_per_places.points_per_third -= initial_points;
                    } else if (initial_points === 5) {
                        doc.points.points_per_places.points_per_fourth -= initial_points;
                    } else if (initial_points === 4) {
                        doc.points.points_per_places.points_per_fifth -= initial_points;
                    } else if (initial_points === 3) {
                        doc.points.points_per_places.points_per_sixth -= initial_points;
                    } else if (initial_points === 2) {
                        doc.points.points_per_places.points_per_seventh -= initial_points;
                    } else if (initial_points === 1) {
                        doc.points.points_per_places.points_per_eighth -= initial_points;
                    }
                    Event.getEventByEvent_name(event_name, (err, event) => {
                        if (err) throw err;
                        event.results.forEach(result => {
                            if (result.athletes_place === 1 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 10;
                                doc.points.points_per_places.points_per_first += 10;
                                doc.points.points_total += 10;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 2 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 8;
                                doc.points.points_per_places.points_per_second += 8;
                                doc.points.points_total += 8;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 3 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 6;
                                doc.points.points_per_places.points_per_third += 6;
                                doc.points.points_total += 6;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 4 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 5;
                                doc.points.points_per_places.points_per_fourth += 5;
                                doc.points.points_total += 5;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 5 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 4;
                                doc.points.points_per_places.points_per_fifth += 4;
                                doc.points.points_total += 4;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 6 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 3;
                                doc.points.points_per_places.points_per_sixth += 3;
                                doc.points.points_total += 3;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 7 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 2;
                                doc.points.points_per_places.points_per_seventh += 2;
                                doc.points.points_total += 2;
                                doc.save();
                                return;
                            } else if (result.athletes_place === 8 && prediction.predicted_winner === result.athletes_name) {
                                prediction.points_for_prediction = 1;
                                doc.points.points_per_places.points_per_eighth += 1;
                                doc.points.points_total += 1;
                                doc.save();
                                return;
                            } else {
                                doc.save();
                            }
                        });
                    });
                }
            });
        });
    });
}
