const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const database_config = require('../config/database');
const passport = require('passport');

const User = require('../models/user');
const Event = require('../models/event');

router.post('/register-user', (req, res) => {
    let newUser = new User(req.body);
    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, msg: 'Failed to register user' });
        } else {
            res.json({ success: true, msg: 'User registered' });
        }
    });
});

router.post('/authenticate-user', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    User.getUserByUsername(username, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({ success: false, msg: 'User not found' });
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign({ data: user }, database_config.secret, { expiresIn: 300 });
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    msg: 'You are now logged in',
                    id: user._id
                });
            } else {
                res.json({
                    success: false,
                    msg: 'Wrong password'
                });
            }
        });
    });
});

router.put('/add-predictions', passport.authenticate('jwt', { session: false }), (req, res) => {
    const user_id = req.body.user_id;
    const predictions = req.body.predictions;
    User.addPredictions(user_id, predictions, (err, user) => {
        if (err) {
            res.json({ success: false, msg: 'Failed to add the predictions' });
        } else {
            res.json({ success: true, msg: 'Predictions have been saved' });
        }
    });
});

router.put('/change-predictions', (req, res) => {

    const user_id = req.body.user_id;
    const predictions = req.body.predictions;
    User.changePredictions(user_id, predictions, (err, user) => {
        if (err) {
            res.json({success: false, msg: 'Failed to change prediction(s)'});
        } else {
            res.json({success: true, msg: 'Changes have been saved'});
        }
    });

});

router.get('/user-data', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({
        user: req.user
    });
});

router.get('/users-points', (req, res) => {
    const user_array = [];
    User.find().exec((err, users) => {
        if (err) throw err;
        users.forEach(user => {
            let user_with_points = {
                user: user.name,
                points: user.points.points_total,
                points_per_places: [
                    user.points.points_per_places.points_per_first,
                    user.points.points_per_places.points_per_second,
                    user.points.points_per_places.points_per_third,
                    user.points.points_per_places.points_per_fourth,
                    user.points.points_per_places.points_per_fifth,
                    user.points.points_per_places.points_per_sixth,
                    user.points.points_per_places.points_per_seventh,
                    user.points.points_per_places.points_per_eighth,
                ]
            }
            user_array.push(user_with_points);
        });
        res.json(user_array);
    });
});

module.exports = router;
