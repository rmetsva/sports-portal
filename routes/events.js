const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');

const Event = require('../models/event');
const User = require('../models/user');

router.post('/add-event', passport.authenticate('jwt', {session:false}), (req, res) => {
    let newEvent = new Event(req.body);
    Event.addEvent(newEvent, (err, event) => {
        if(err) {
            res.json({success: false, msg: 'Failed to add the event'});
        } else {
            res.json({success: true, msg: 'Event added'});
        }
    });
});

router.put('/add-results', passport.authenticate('jwt', {session:false}), (req, res) => {
    const event_name = req.body.event_name;
    const results = req.body.results;
    Event.addResults(event_name, results, (err, event) => {
        if(err) {
            res.json({success: false, msg: 'Failed to add the results'});
        } else {
            res.json({success: true, msg: 'Results have been added'});
            User.calculatePoints(event_name);
        }
    });
});

router.get('/get-event-info', passport.authenticate('jwt', {session:false}), (req, res) => {
    let suggestion_array = [];
    Event.find().exec((err, docs) => {
        if (err) throw err;
        docs.forEach(doc => {
            let suggestions_for_event = {
                event_name: doc.event_name,
                suggestions: doc.participants
            }
            suggestion_array.push(suggestions_for_event);
        });
        res.json({
            suggestions: suggestion_array
        });
    });
});

module.exports = router;
