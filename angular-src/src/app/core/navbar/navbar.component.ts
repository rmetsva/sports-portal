import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthService } from '../../auth/auth.service';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private navLinks: NavLink[];

  constructor(private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService) { }

  ngOnInit() {
    this.navLinks = [{
      path: 'login',
      label: 'Login'
    },
    {
      path: 'register',
      label: 'Register'
    },
    {
      path: 'profile',
      label: 'Profile'
    },
    {
      path: 'prediction-area',
      label: 'Predictions'
    },
    {
      path: 'ranking',
      label: 'Ranking'
    },
    {
      path: 'admin',
      label: 'Admin'
    }];
  }

  isLinkVisible(path) {
    if (path === 'login' || path === 'register') {
      return !this.isAuthenticated();
    } else if (path === 'profile' || path === 'prediction-area' || path === 'logout' || path === 'ranking') {
      return this.isAuthenticated();
    } else if (path === 'admin') {
      if (this.isAuthenticated()) {
        return this.isAdmin();
      } else {
        return false;
      }
    }
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  isAdmin() {
    return this.userService.isAdmin();
  }

  onLogout() {
    this.authService.logout();
    this.flashMessage.show('You are now logged out', { cssClass: 'alert-neutral', timeout: 3000 });
    this.router.navigate(['/login']);
  }

}

interface NavLink {
  path: String;
  label: String;
}
