import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material';

import { NavbarComponent } from './navbar/navbar.component';

import { AppRoutingModule } from '../app-routing.module';

@NgModule({
    declarations: [
        NavbarComponent
    ],
    imports: [
        AppRoutingModule,
        MatTabsModule,
        CommonModule
    ],
    exports: [
        NavbarComponent
    ]
})
export class CoreModule {}
