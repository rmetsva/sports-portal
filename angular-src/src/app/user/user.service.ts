import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { AuthService } from '../auth/auth.service';

@Injectable()
export class UserService {

  constructor(private http: Http, private authService: AuthService) { }

  getUserData() {
    const headers = new Headers();
    const token = this.authService.loadToken();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/user-data', {headers: headers})
      .map(res => res.json());
  }

  addPredictions(predictions) {
    const headers = new Headers();
    const token = this.authService.loadToken();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    const id = this.authService.loadID();
    const body = {
      user_id: id,
      predictions: predictions
    };
    return this.http.put('http://localhost:3000/users/add-predictions', body, {headers: headers})
      .map(res => res.json());
  }

  getUsersPoints() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/users-points', {headers: headers})
      .map(res => res.json());
  }

  isAdmin() {
    if (localStorage.getItem('id') === '5a0ad60f942a3f220077a257') {
      return true;
    } else {
      return false;
    }
  }

}
