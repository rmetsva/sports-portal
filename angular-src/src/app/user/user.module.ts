import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatInputModule, MatButtonModule,
MatTableModule, MatListModule, MatProgressSpinnerModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ProfileComponent } from './profile/profile.component';
import { PredictionsComponent } from './prediction-area/predictions/predictions.component';
import { MyPredictionsComponent } from './prediction-area/my-predictions/my-predictions.component';
import { RankingComponent } from './ranking/ranking.component';
import { PredictionAreaComponent } from './prediction-area/prediction-area.component';

import { UserService } from './user.service';

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    CommonModule,
    MatAutocompleteModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatExpansionModule,
    BrowserAnimationsModule
  ],
  declarations: [
    ProfileComponent,
    PredictionsComponent,
    MyPredictionsComponent,
    RankingComponent,
    PredictionAreaComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
