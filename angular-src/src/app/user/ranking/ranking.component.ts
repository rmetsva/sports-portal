import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { UserService } from '../user.service';

export class RankingDataSource extends DataSource<any> {

  constructor(private usersPoints: any[]) {
    super();
  }

  connect(): Observable<Element[]> {

    return Observable.of(this.usersPoints);
  }

  disconnect() { }

}

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {
  usersPoints: any[];
  displayedColumns = ['user', 'points'];
  dataSource: RankingDataSource | null;

  mode = 'indeterminate';
  diameter = 65;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsersPoints().subscribe(data => {
      this.usersPoints = data.sort(this.compareFn);
      this.dataSource = new RankingDataSource(this.usersPoints);
    });
  }

  compareFn = (a, b) => {
    if (a.points < b.points) {
      return 1;
    } else if (a.points > b.points) {
      return -1;
    } else {
      for (let i = 0; i < 8; i++) {
        if (a.points_per_places[i] < b.points_per_places[i]) {
          return 1;
        } else if (a.points_per_places[i] > b.points_per_places[i]) {
          return -1;
        }
        return 1;
      }
    }
  }

}
