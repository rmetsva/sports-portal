import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule, MatButtonModule, MatListModule, MatAutocompleteModule } from '@angular/material';

import { EventsComponent } from './events/events.component';
import { ResultsComponent } from './results/results.component';
import { AdminComponent } from './admin/admin.component';

import { AdminService } from './admin.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatAutocompleteModule
  ],
  declarations: [
    EventsComponent,
    ResultsComponent,
    AdminComponent
  ],
  providers: [
    AdminService
  ]
})
export class AdminModule { }
