import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { AuthService } from '../auth/auth.service';

@Injectable()
export class AdminService {

  constructor(private http: Http, private authService: AuthService) { }

  addEvent(event: Object) {
    const headers = new Headers();
    const token = this.authService.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return this.http.post('http://localhost:3000/events/add-event', event, {headers: headers})
      .map(res => res.json());
  }

  addResults(results: Object) {
    const headers = new Headers();
    const token = this.authService.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return this.http.put('http://localhost:3000/events/add-results', results, {headers: headers})
      .map(res => res.json());
  }

  getEventInfo() {
    const headers = new Headers();
    const token = this.authService.loadToken();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return this.http.get('http://localhost:3000/events/get-event-info', {headers: headers})
      .map(res => res.json());
  }

}
