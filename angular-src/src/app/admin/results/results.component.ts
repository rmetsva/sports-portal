import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { AdminService } from '../admin.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  eventName: String;
  place: number = 1;
  athlete: String;
  results: Result[] = [];
  eventInfoList: any[];

  eventFormControl: FormControl;
  athleteFormControl: FormControl;

  eventSuggestions: String[] = [];
  filteredEventSuggestions: Observable<String[]>;
  athleteSuggestions: String[] = [];
  filteredAthleteSuggestions: Observable<String[]>;

  constructor(private adminService: AdminService,
    private flashMessage: FlashMessagesService,
    private router: Router) { }

  ngOnInit() {
    this.eventFormControl = new FormControl();
    this.athleteFormControl = new FormControl();
    this.adminService.getEventInfo().subscribe(info => {
      this.eventInfoList = info.suggestions;

      this.eventInfoList.forEach(eventInfo => {
        this.eventSuggestions.push(eventInfo.event_name);
      });

      this.filteredEventSuggestions = this.eventFormControl.valueChanges
        .map(suggestion => {
          this.eventInfoList.forEach(eventInfo => {
            if (suggestion === eventInfo.event_name) {
              this.athleteSuggestions = eventInfo.suggestions;
            }
          });
          this.results = [];
          this.place = 1;
          this.athlete = '';
          return suggestion ? this.filterSuggestions(suggestion, this.eventSuggestions) : this.eventSuggestions.slice();
        });
    });
    this.filteredAthleteSuggestions = this.athleteFormControl.valueChanges
      .startWith(null)
      .map(suggestion => suggestion ? this.filterSuggestions(suggestion, this.athleteSuggestions) : this.athleteSuggestions.slice());

  }

  filterSuggestions(name: String, suggestions: String[]) {
    return suggestions.filter(suggestion =>
      suggestion.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  onAddResult() {
    const result: Result = {
      athletes_place: this.place,
      athletes_name: this.athlete
    };
    this.results.push(result);
    const index = this.athleteSuggestions.indexOf(this.athlete);
    if (index > -1) {
      this.athleteSuggestions.splice(index, 1);
    }
    if (this.place < 8) {
      this.place++;
    }
    this.athlete = '';
  }

  onAddResultsSubmit() {
    const results = {
      event_name: this.eventName,
      results: this.results
    };

    this.adminService.addResults(results).subscribe(data => {
      this.flashMessage.show(data.msg, { cssClass: 'alert-success', timeout: 3000 });
      this.eventName = '';
      this.place = 1;
      this.results = [];
    }, err => {
      this.flashMessage.show('Your session has expired, log in again to add results', {cssClass: 'alert-danger', timeout: 3000});
      this.router.navigate(['/login']);
      localStorage.clear();
    });
  }
}

interface Result {
  athletes_place: Number;
  athletes_name: String;
}


