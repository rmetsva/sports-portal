import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  eventName: String;
  participant: String;
  participants: String[] = [];

  constructor(private adminService: AdminService,
              private router: Router,
              private flashMessage: FlashMessagesService) { }

  ngOnInit() {
  }

  onAddParticipant() {
    this.participants.push(this.participant);
    this.participant = '';
  }

  onAddEventSubmit() {
    const event = {
      event_name: this.eventName,
      participants: this.participants
    };

    this.adminService.addEvent(event).subscribe(data => {
      this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 3000});
      this.eventName = '';
      this.participants = [];
    }, err => {
      this.flashMessage.show('Your session has expired, log in again to add new events', {cssClass: 'alert-danger', timeout: 3000});
      this.router.navigate(['/login']);
      localStorage.clear();
    });

  }

}
