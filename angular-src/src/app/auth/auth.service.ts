import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  authToken: any;

  constructor(private http: Http) { }

  registerUser(user) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/register-user', user, {headers: headers})
      .map(res => res.json());
  }

  authenticateUser(user) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate-user', user, {headers: headers})
      .map(res => res.json());
  }

  logout() {
    this.authToken = null;
    localStorage.clear();
  }

  storeUserData(token, id) {
    localStorage.setItem('token', token);
    localStorage.setItem('id', id);
    this.authToken = token;
  }

  isAuthenticated() {
    return tokenNotExpired();
  }

  loadToken() {
    const token = localStorage.getItem('token');
    this.authToken = token;
    return token;
  }

  loadID() {
    return localStorage.getItem('id');
  }

}
