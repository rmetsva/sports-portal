import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AdminComponent } from '../admin/admin/admin.component';

import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService,
                private userService: UserService,
                private router: Router,
                private flashMessage: FlashMessagesService) { }

    canActivate(route: ActivatedRouteSnapshot) {

        if (this.authService.isAuthenticated()) {
            if (route.component === AdminComponent) {
                return this.userService.isAdmin();
            } else {
                return true;
            }
        } else {
            this.flashMessage.show('Your session has expired - log in again', {cssClass: 'alert-danger', timeout: 3000});
            this.router.navigate(['/login']);
            localStorage.clear();
            return false;
        }
    }
}
